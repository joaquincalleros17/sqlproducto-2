package database;

import java.io.Serializable;

public class Producto implements Serializable {
    private long _ID;
    private int codigo;
    private String nombre;
    private String marca;
    private float precio;
    private int duracion;

    public Producto(int codigo, String nombre, String marca, int duracion, long precio) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.marca = marca;
        this.duracion = duracion;
        this.precio = precio;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Producto() {

    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
}
