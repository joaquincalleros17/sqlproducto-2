package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ProductosDbHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE= " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA = " ,";
    private static final String SQL_CREATE_CONTACTO = " CREATE TABLE " +
            DefinirTabla.Producto.TABLA_NAME + " (" +
            DefinirTabla.Producto.CODIGO + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Producto.NOMBRE + TEXT_TYPE + COMMA +
            DefinirTabla.Producto.MARCA + TEXT_TYPE + COMMA +
            DefinirTabla.Producto.PRECIO + TEXT_TYPE + COMMA +
            DefinirTabla.Producto.DURACION + TEXT_TYPE + COMMA + ")";

    private static final String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " + DefinirTabla.Producto.TABLA_NAME;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "producto.db";

    public ProductosDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null,DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CONTACTO);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);
    }
}
