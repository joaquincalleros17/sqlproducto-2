package com.example.sqlproducto_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import database.DbProductos;
import database.Producto;

public class ProductoActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioButton rdPerecedero;
    private RadioButton rdNoPerecedero;
    private Button btnActualizar;
    private Button btnBuscar;
    private Button btnBorrar;
    private Button btnLimpiar;

    private DbProductos db;
    private Producto producto;
    private Producto savedProduct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        txtCodigo = findViewById(R.id.txtCodigo);
        btnBuscar = findViewById(R.id.btnBuscar);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rdPerecedero = findViewById(R.id.rdPerecedero);
        rdNoPerecedero = findViewById(R.id.rdPerecedero);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        db = new DbProductos(this);

        btnBorrar.setEnabled(false);
        btnActualizar.setEnabled(false);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtCodigo.getText().toString().matches("")) {
                    Toast.makeText(ProductoActivity.this, "Inserte un código", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        db.openDataBase();
                        producto = db.getProducto(Integer.parseInt(txtCodigo.getText().toString()));
                        db.cerrarDataBase();
                        savedProduct = producto;
                        if (producto != null) {
                            btnBorrar.setEnabled(true);
                            btnActualizar.setEnabled(true);
                            txtNombre.setText(producto.getNombre());
                            txtMarca.setText(producto.getMarca());
                            txtPrecio.setText(String.valueOf(producto.getPrecio()));
                            if (producto.getDuracion() > 0) {
                                rdPerecedero.setChecked(true);
                            } else
                                rdNoPerecedero.setChecked(true);
                        } else {
                            Toast.makeText(ProductoActivity.this, "No se encontraron resultados", Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                    }
                    catch (Exception e)
                    {
                        Toast.makeText(ProductoActivity.this, "No se encontraron resultados", Toast.LENGTH_SHORT).show();
                        limpiar();
                    }

                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombre.getText().toString().matches("")  || txtMarca.getText().toString().matches("") ||
                        txtPrecio.getText().toString().matches("") || txtCodigo.getText().toString().matches("")) {
                    Toast.makeText(ProductoActivity.this, "Fantan campos por llenar",Toast.LENGTH_SHORT).show();
                } else {
                    Producto product = new Producto();
                    product.setCodigo(savedProduct.getCodigo());
                    product.setNombre(txtNombre.getText().toString());
                    product.setMarca(txtMarca.getText().toString());
                    product.setPrecio(Float.parseFloat(txtPrecio.getText().toString()));
                    product.setDuracion(rdPerecedero.isChecked() ? 1 : 0);

                    db.openDataBase();
                    if(db.updateProducto(product,product.getCodigo()) > -1) {
                        Toast.makeText(ProductoActivity.this, "Se actualizó", Toast.LENGTH_SHORT).show();
                        btnBorrar.setEnabled(false);
                        btnActualizar.setEnabled(false);
                    } else {
                        Toast.makeText(ProductoActivity.this, "Rrror", Toast.LENGTH_SHORT).show();
                    }
                    db.cerrarDataBase();
                }
                limpiar();
            }
        });
        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();

                db.deleteProducto(Integer.parseInt(txtCodigo.getText().toString()));
                Toast.makeText(ProductoActivity.this, "Se ha eliminado con éxito el producto con ID  " + Long.parseLong(txtCodigo.getText().toString()), Toast.LENGTH_SHORT).show();
                db.cerrarDataBase();
                btnBorrar.setEnabled(false);
                btnActualizar.setEnabled(false);
                limpiar();
            }
        });
    }
    private void limpiar(){
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtCodigo.setText("");
        rdPerecedero.setChecked(true);

    }
}
