package com.example.sqlproducto_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import database.DbProductos;
import database.Producto;

public class MainActivity extends AppCompatActivity {
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioButton rbPerecedero;
    private RadioButton rbNoPerecedero;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnEditar;

    private DbProductos db;
    private Producto savedProduct;
    private long id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCodigo = (EditText) findViewById(R.id.txtCodigo);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtMarca = (EditText) findViewById(R.id.txtMarca);
        txtPrecio = (EditText) findViewById(R.id.txtPrecio);
        rbPerecedero = findViewById(R.id.rdPerecedero);
        rbNoPerecedero = findViewById(R.id.rdNoPerecedero);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnEditar = (Button) findViewById(R.id.btnIrEditar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        db = new DbProductos(MainActivity.this);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarCampos() == false){
                    Toast.makeText(MainActivity.this, "Llenar todos los campos", Toast.LENGTH_SHORT).show();
                }else{
                    Producto p= new Producto(Integer.parseInt(txtCodigo.getText().toString()),
                            txtNombre.getText().toString(),
                            txtMarca.getText().toString(),
                            (rbPerecedero.isChecked() ? 1:0),
                            Long.parseLong(txtPrecio.getText().toString()));
                    db.openDataBase();
                    try {
                        Producto temporal = db.getProducto(Integer.parseInt(txtCodigo.getText().toString()));
                        if (temporal == null) {

                            if (savedProduct != null) {
                                long id = db.updateProducto(p, savedProduct.getCodigo());
                                Toast.makeText(MainActivity.this, "El producto se actualizó: " + id, Toast.LENGTH_SHORT).show();
                                savedProduct = null;
                            } else {
                                long id = db.insertarProducto(p);
                                if (id > -1)
                                    Toast.makeText(MainActivity.this, "El producto se guardó: " + id, Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(MainActivity.this, "Faltal Error", Toast.LENGTH_SHORT).show();
                            }
                            db.cerrarDataBase();
                            limpiar();
                        } else {
                            Toast.makeText(MainActivity.this, "Codigo en Existencia", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "El código ya existe, error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ProductoActivity.class);
                startActivityForResult(i,0);
            }
        });
    }
    public boolean validarCampos(){
        if(txtCodigo.getText().toString().matches("")) return false;
        if(txtNombre.getText().toString().matches("")) return false;
        if(txtMarca.getText().toString().matches("")) return false;
        if(txtPrecio.getText().toString().matches("")) return false;
        return true;
    }
    private void limpiar(){
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtCodigo.setText("");
        rbPerecedero.setChecked(true);
    }
}
